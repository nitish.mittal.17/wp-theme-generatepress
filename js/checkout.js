var $ = window.jQuery;
if (window.location.href.indexOf('/checkout') > -1) {
    var $checkoutForm = $('form.woocommerce-checkout');
    setInterval(function() {
        if ($('#place_order').attr('event-binded') !== 'true') {
            $('#place_order').on('click', function() {
                window.localStorage.setItem('shippingAddress', `${$('#billing_first_name').val()} ${$('#billing_last_name').val()}
${$('#billing_address_1').val()}
${$('#billing_address_2').val()}
${$('#billing_city').val()} - ${$('#billing_postcode').val()}
${$('#billing_state').val()}`)
            }).attr('event-binded', "true");
        }
    }, 1000);
    
}

if (window.location.href.indexOf('order-received') > -1) {
    var redirectToWhatsapp = function() {
        var messageString = `New Order received

*Order Id*: ${$('li.woocommerce-order-overview__order.order').find('strong').html()}
*Total Amount*: ${$('.woocommerce-order-overview__total.total').find('.amount').contents().eq(1).text()}/-

*Items:*

`;
$('.order_details').find('tbody').find('.woocommerce-table__line-item').each(function() {
    messageString += `${$(this).find('a').html()} *${$(this).find('strong').html().replace(/&nbsp;/, ' ')}* - Rs. ${$(this).find('.amount').contents().eq(1).text()}

`;
});

messageString += `*Shipping Address:*

${window.localStorage.getItem('shippingAddress')}`;
console.log(messageString);
window.location.href = 'https://api.whatsapp.com/send?phone=919873318282&text=' + encodeURIComponent(messageString);
    };

    redirectToWhatsapp();
}
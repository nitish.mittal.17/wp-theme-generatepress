var $ = window.jQuery;
var $ = window.jQuery;
if (screen.width < 500) {
    window.shareExternalHandler = function() {
        if (window.navigator.share) {
            var url = document.location.href;
	        var canonicalElement = document.querySelector('link[rel=canonical]');
	        if (canonicalElement !== null) {
		        url = canonicalElement.href;
	        }
	        navigator.share({url: url});
        }
    }
    $('body').append(`
    <div class="footer-links-container">
        <ul>
            <li class="whatsapp"><a href="https://api.whatsapp.com/send?phone=919873318282&text=Hi"><i class="fab fa-whatsapp"></i><span>Whatsapp</span></a></li>
            <li class="callus"><a href="tel:+919873318282"><i class="fa fa-phone fa-rotate-90"></i><span>Call us</span></a></li>
            <li class="map"><a href="https://www.google.com/maps?saddr=My+Location&amp;daddr=28.887128399999998,76.63305559999999"><i class="fa fa-map-marker"></i><span>Map</span></a></li>
            <li class="share"><a href="#" onclick="shareExternalHandler();"><i class="fa fa-share"></i><span>Share</span></a></li>
        </ul>
    </div>
    `);
    $('body').append(`<div style="height: ${$('.footer-links-container').outerHeight()}px"></div>`);
}